import 'package:flutter/material.dart';

class Character {
  final String name;
  final String imagePath;
  final String description;
  final List<Color> colors;

  Character({this.name, this.imagePath, this.description, this.colors});
}

List characters = [
  Character(
    name: "User1",
    imagePath: "assets/images/images#1.png",
    description:
        "Enim reprehenderit enim tempor elit amet quis enim sunt exercitation quis amet voluptate. Tempor Lorem cupidatat consequat esse ipsum Lorem reprehenderit aliquip tempor nostrud ullamco magna veniam ut. Eiusmod nulla eu velit ipsum aliqua Lorem cupidatat amet laboris veniam in sint qui. Deserunt ipsum fugiat incididunt laborum in sint. Culpa esse qui et ullamco aute in exercitation nulla cillum ea.",
    colors: [Colors.orange.shade200, Colors.deepOrange.shade400],
  ),
  Character(
    name: "User2",
    imagePath: "assets/images/images#2.png",
    description:
        "Enim reprehenderit enim tempor elit amet quis enim sunt exercitation quis amet voluptate. Tempor Lorem cupidatat consequat esse ipsum Lorem reprehenderit aliquip tempor nostrud ullamco magna veniam ut. Eiusmod nulla eu velit ipsum aliqua Lorem cupidatat amet laboris veniam in sint qui. Deserunt ipsum fugiat incididunt laborum in sint. Culpa esse qui et ullamco aute in exercitation nulla cillum ea.",
    colors: [Colors.pink.shade200, Colors.redAccent.shade400],
  ),
  Character(
    name: "User3",
    imagePath: "assets/images/images#3.png",
    description:
        "Enim reprehenderit enim tempor elit amet quis enim sunt exercitation quis amet voluptate. Tempor Lorem cupidatat consequat esse ipsum Lorem reprehenderit aliquip tempor nostrud ullamco magna veniam ut. Eiusmod nulla eu velit ipsum aliqua Lorem cupidatat amet laboris veniam in sint qui. Deserunt ipsum fugiat incididunt laborum in sint. Culpa esse qui et ullamco aute in exercitation nulla cillum ea.",
    colors: [Colors.blue.shade200, Colors.blueAccent.shade400],
  ),
  Character(
    name: "User4",
    imagePath: "assets/images/images#4.png",
    description:
        "Enim reprehenderit enim tempor elit amet quis enim sunt exercitation quis amet voluptate. Tempor Lorem cupidatat consequat esse ipsum Lorem reprehenderit aliquip tempor nostrud ullamco magna veniam ut. Eiusmod nulla eu velit ipsum aliqua Lorem cupidatat amet laboris veniam in sint qui. Deserunt ipsum fugiat incididunt laborum in sint. Culpa esse qui et ullamco aute in exercitation nulla cillum ea.",
    colors: [Colors.green.shade200, Colors.lightGreen.shade400],
  ),
  Character(
    name: "User5",
    imagePath: "assets/images/images#5.png",
    description:
        "Enim reprehenderit enim tempor elit amet quis enim sunt exercitation quis amet voluptate. Tempor Lorem cupidatat consequat esse ipsum Lorem reprehenderit aliquip tempor nostrud ullamco magna veniam ut. Eiusmod nulla eu velit ipsum aliqua Lorem cupidatat amet laboris veniam in sint qui. Deserunt ipsum fugiat incididunt laborum in sint. Culpa esse qui et ullamco aute in exercitation nulla cillum ea.",
    colors: [Colors.purple.shade200, Colors.deepPurple.shade400],
  ),
  Character(
    name: "User6",
    imagePath: "assets/images/images#6.png",
    description:
        "Enim reprehenderit enim tempor elit amet quis enim sunt exercitation quis amet voluptate. Tempor Lorem cupidatat consequat esse ipsum Lorem reprehenderit aliquip tempor nostrud ullamco magna veniam ut. Eiusmod nulla eu velit ipsum aliqua Lorem cupidatat amet laboris veniam in sint qui. Deserunt ipsum fugiat incididunt laborum in sint. Culpa esse qui et ullamco aute in exercitation nulla cillum ea.",
    colors: [Colors.grey.shade200, Colors.blueGrey.shade400],
  ),
];
