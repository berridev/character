import 'package:character/pages/characterlist.dart';
import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(
        home: CharacterListScreen(),
        title: "Karakter",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Colors.white,
          canvasColor: Colors.white ,
          appBarTheme: AppBarTheme(
            color: Colors.white,
            elevation: 0,
          ),
        ),
      ),
    );
