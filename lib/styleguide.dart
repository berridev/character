import 'package:flutter/material.dart';

class AppTheme {
  static const TextStyle display1 = TextStyle(
      fontFamily: "WorkSans",
      color: Colors.black,
      fontSize: 30,
      fontWeight: FontWeight.w600,
      letterSpacing: 1.1);

  static const TextStyle display2 = TextStyle(
      fontFamily: "WorkSans",
      color: Colors.black,
      fontSize: 26,
      fontWeight: FontWeight.normal,
      letterSpacing: 1.0);

  static final TextStyle heading = TextStyle(
      fontFamily: "WorkSans",
      color: Colors.white.withOpacity(0.8),
      fontSize: 34,
      fontWeight: FontWeight.w700,
      letterSpacing: 1.2);

  static final TextStyle subHeading = TextStyle(
      inherit: true,
      fontFamily: "WorkSans",
      color: Colors.white.withOpacity(0.8),
      fontSize: 20,
      fontWeight: FontWeight.w500);
}
